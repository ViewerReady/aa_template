﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarInteract : MonoBehaviour
{

    private AudioSource audioSource;
    private bool isPlaying;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        isPlaying = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setAudioStatus()
    {
        if (!isPlaying)
        {
            audioSource.Play();
            isPlaying = true;
        }
        else
        {
            audioSource.Pause();
            isPlaying = false;
        }

    }



}
