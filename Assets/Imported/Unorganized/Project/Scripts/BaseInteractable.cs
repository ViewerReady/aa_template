using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaseInteractable : MonoBehaviour
{
    public UnityEvent OnRaycastTrigger = new UnityEvent();

    public void OnRaycastHit() {
        Debug.Log("Ray Hit");
        OnRaycastTrigger.Invoke();
    }
}
