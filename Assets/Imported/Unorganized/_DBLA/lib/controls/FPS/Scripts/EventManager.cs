using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using UnityEngine.Events;
using System.Globalization;

public class EventManager : MonoBehaviour
{
    public static int _timeCheckInterval = 1;

    [SerializeField]
    private string _currentTime;

    [SerializeField]
    private List<Show> Shows;

    private TimeSpan _currentDateTime;

    private TimeData _timeData;

    private const string _endpoint = "https://worldtimeapi.org/api/timezone/America/Chicago.json";

    IEnumerator CheckTime()
    {

        using (UnityWebRequest www = UnityWebRequest.Get(_endpoint))
        {

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {

                Debug.Log(www.error);

                _currentDateTime = DateTime.Now.TimeOfDay;
            }
            else
            {

                _timeData = JsonUtility.FromJson<TimeData>(www.downloadHandler.text);

                _currentTime = _timeData.datetime;

                _currentDateTime = Convert.ToDateTime(_currentTime).TimeOfDay;
            }
        }
    }

    private void Start()
    {

        StartCoroutine(Initialize());
    }

    private void EventRunner()
    {

        StartCoroutine(CheckTime());

        foreach (Show show in Shows)
        {

            if (_currentDateTime.TotalMilliseconds >= show._startTime.TotalMilliseconds && _currentDateTime.TotalMilliseconds <= show._endTime.TotalMilliseconds)
            {

                show.ShowRunner(_currentDateTime);
            }
        }
    }

    IEnumerator Initialize()
    {

        yield return new WaitForSeconds(5);

        InvokeRepeating("EventRunner", 0, _timeCheckInterval);
    }
}

[Serializable]
public class Show
{
    [NonSerialized]
    private bool ShowInProgress = false;

    public string ShowName;

    public string StartTime;

    public TimeSpan _startTime { get { return DateTime.ParseExact(StartTime, "hh:mm:sstt", CultureInfo.InvariantCulture).TimeOfDay; } }

    public string EndTime;

    public TimeSpan _endTime { get { return DateTime.ParseExact(EndTime, "hh:mm:sstt", CultureInfo.InvariantCulture).TimeOfDay; } }

    public UnityEvent OnCurrentShow = new UnityEvent();

    public void ShowRunner(TimeSpan currentDateTime)
    {

        TimeSpan timeElasped = _startTime.Add(currentDateTime);

        if ((int)timeElasped.TotalMilliseconds >= (EventManager._timeCheckInterval * 1000) && !ShowInProgress)
        {

            SetShowState();
        }
    }

    public void SetShowState()
    {

        ShowInProgress = true;

        Debug.Log("Show " + ShowName + " In Progress");

        OnCurrentShow.Invoke();
    }
}

[Serializable]
public class TimeData
{
    public string abbreviation;

    public string client_ip;

    public string datetime;

    public int day_of_week;

    public int day_of_year;

    public string dst;

    public string dst_from;

    public int dst_offset;

    public string dst_until;

    public int raw_offset;

    public string timezone;

    public int unixtime;

    public string utc_datetime;

    public string utc_offset;

    public int week_number;
}