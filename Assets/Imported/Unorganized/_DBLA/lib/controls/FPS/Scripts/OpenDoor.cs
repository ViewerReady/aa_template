﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{

    public GameObject leftDoor;
    public GameObject rightDoor;    
    public bool doorIsOpening;



    // Start is called before the first frame update
    void Start()
    {
        doorIsOpening = false;       
        
    }

    // Update is called once per frame
    void Update()
    {
        if(doorIsOpening == true)
        {
            
            leftDoor.transform.Translate(Vector3.right * Time.deltaTime * 3);
            rightDoor.transform.Translate(Vector3.left * Time.deltaTime * 3);
        }
        if(leftDoor.transform.position.x > 3f)
        {
            doorIsOpening = false;           

        }
        
    }
    void OnTriggerEnter(Collider other)
    {
        doorIsOpening = true;
    }

    
}
