﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Analytics;

public class VoteManager : MonoBehaviour
{
    private string _endpoint = "https://viper-d0d62.firebaseio.com/voters/";

    public static List<VoteInteractable> VoteInteractables = new List<VoteInteractable>();

    public int CurrentVoteID;

    public void SubmitToRemote()
    {
        if (CurrentVoteID != 0)
        {

            Debug.Log("You have submitted " + CurrentVoteID);

            User userVote = new User(CurrentVoteID);

            StartCoroutine(UploadVote(userVote));
        }

        else
        {

            Debug.Log("You havent voted!");
        }
    }

    private void OnEnable()
    {
        VoteInteractable.VoteManager = this;
    }



    public void ActivateVote()
    {
        foreach (VoteInteractable voteInteractable in VoteInteractables)
        {
            if (voteInteractable.PictureID == CurrentVoteID)
            {

                voteInteractable.VoteAnimator.SetTrigger("Vote");

                voteInteractable.VoteAnimator.ResetTrigger("Reset");
            }
            else
            {

                voteInteractable.VoteAnimator.ResetTrigger("Vote");

                voteInteractable.VoteAnimator.SetTrigger("Reset");
            }
        }
    }

    IEnumerator UploadVote(User userVote)
    {

        string json = JsonUtility.ToJson(userVote);

        using (UnityWebRequest www = UnityWebRequest.Put(_endpoint + AnalyticsSessionInfo.userId + ".json", json))
        {

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {

                Debug.Log(www.error);
            }
            else
            {

                Debug.Log("Upload complete!");
            }
        }
    }
}

[Serializable]
public class User
{
    public int VoteID;

    public User(int voteId)
    {

        this.VoteID = voteId;
    }
}
