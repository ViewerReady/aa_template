﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class DisplayContentCycle : MonoBehaviour
{

    public VideoPlayer videoPlayer;
    public BoxCollider boxCollider;
    public GameObject[] pointers;

    // Start is called before the first frame update
    void Start()
    {
        //boxCollider = this.GetComponent<BoxCollider>();
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void playVideo()
    {

        videoPlayer.Play();
        boxCollider.enabled = false;

        for (int i = 0; i < pointers.Length; i++)
        {
            pointers[i].SetActive(true);
        }
        
    }


}
