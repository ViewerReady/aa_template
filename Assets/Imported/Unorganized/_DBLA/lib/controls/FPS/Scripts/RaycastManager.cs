using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastManager : MonoBehaviour
{
    void Update() {

        if(Input.GetMouseButtonDown(0)) {

            if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, 100.0f)) {

                if(hit.collider.TryGetComponent(out BaseInteractable baseInteractable)) {

                    baseInteractable.OnRaycastHit();
                }
            }
        }
    }
}
