﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MultiColorSelector : MonoBehaviour
{
    public List<Color> Colors;
    private GameObject player;

    [Range(1, 3)]
    public float TransitionSpeed = 2f;
    private int _counter;
    private AudioSource audioSource;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {

        if (Colors.Any())
        {

            GetComponent<Renderer>().material.SetColor("_EmissionColor", Colors[0]);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                
                if (hit.transform.gameObject.name == this.gameObject.name
                    && Vector3.Distance(player.transform.position, hit.transform.position) <= 10)
                {
                    changeColor();
                    playSound();
                }
            }
        }
    }

    private void playSound()
    {
        if (audioSource != null)
        {
            audioSource.Play();
        }
    }


    public void changeColor()
    {

        if (Colors.Count > 1)
        {

            int index = (int)Mathf.Repeat(_counter++, Colors.Count);

            StartCoroutine(LerpColorsOverTime(Colors[index], Colors[(int)Mathf.Repeat(index + 1, Colors.Count)], TransitionSpeed));

            if (_counter >= Colors.Count)
            {

                _counter = 0;
            }
        }
    }

    private IEnumerator LerpColorsOverTime(Color startingColor, Color endingColor, float speed)
    {

        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime * speed)
        {
            for (int i = 0; i < GetComponent<Renderer>().materials.Length; i++)
            {
                GetComponent<Renderer>().materials[i].SetColor("_EmissionColor", Color.Lerp(startingColor, endingColor, t));
            }

            yield return null;
        }
    }
}
