﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VoteInteractable : MonoBehaviour
{
    public int PictureID;

    public static VoteManager VoteManager;

    public Animator VoteAnimator;

    public TextMeshPro TextID;

    private void Awake()
    {
        TextID.text = PictureID.ToString();
    }

    public void vote()
    {
        VoteManager.CurrentVoteID = PictureID;
        VoteManager.ActivateVote();
    }

    private void OnEnable()
    {
        VoteManager.VoteInteractables.Add(this);
    }

    private void OnDisable()
    {
        VoteManager.VoteInteractables.Remove(this);
    }
}
