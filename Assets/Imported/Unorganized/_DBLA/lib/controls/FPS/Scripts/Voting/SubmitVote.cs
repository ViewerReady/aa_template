﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SubmitVote : MonoBehaviour
{

    private Color color;

    public void hasVoted()
    {
        GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
    }
}
