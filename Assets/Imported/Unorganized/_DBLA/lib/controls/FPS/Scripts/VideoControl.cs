﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoControl : MonoBehaviour
{

    VideoPlayer videoPlayer;
       
    

    private void Awake()
    {
        videoPlayer = this.GetComponent<VideoPlayer>();
    }


    public void Play()
    {
        foreach (VideoControl video in FindObjectsOfType<VideoControl>())
        {
            if (video != this)
            {
                video.videoPlayer.Pause();
            }
           
        }

        if (videoPlayer.isPlaying)
        {
            videoPlayer.Pause();
        }
        else
        {
            videoPlayer.Play();
        }
    }

    /*private void OnMouseDown()
    {
        Play();
    }*/
}
