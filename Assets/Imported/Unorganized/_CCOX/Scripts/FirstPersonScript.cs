﻿using UnityEngine;


[SelectionBase]
public class FirstPersonScript : MonoBehaviour
{
    public float mouseSensitivity = 100;
    public Transform PlayerBody;
    public CharacterController controller;
    public Camera CameraObject;
    public float MoveSpeed = 5;
    public float SprintSpeed = 8;
    public float gravity = .9f;

    private float fall_speed = 0;
    private float speed = 0;
    private float xRot = 0;

    // Start is called before the first frame update
    private void Start()
    {
        // --------- Lock Mouse Cursor
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    private void Update()
    {
        // --------- Update Player Mouse Look --------- 
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        bool kbShift = Input.GetKey(KeyCode.LeftShift);

        xRot -= mouseY;
        xRot = Mathf.Clamp(xRot, -90, 90);

        if (!kbShift)
        {
            speed = MoveSpeed;
        }
        else
        {
            speed = SprintSpeed;
        }

        CameraObject.transform.localRotation = Quaternion.Euler(xRot, 0, 0);
        PlayerBody.Rotate(Vector3.up * mouseX);

        // --------- Update Player Movement --------- 
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        fall_speed -= gravity * Time.deltaTime;

        Vector3 move = (transform.right * x) + (transform.forward * z);
        ///controller.Move(move * speed * Time.deltaTime);

        controller.Move(move * speed * Time.deltaTime);

        controller.Move(new Vector3(0, fall_speed, 0));

        if (controller.isGrounded) fall_speed = 0;

    }
}