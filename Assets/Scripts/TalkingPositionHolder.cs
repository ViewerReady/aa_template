﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingPositionHolder : MonoBehaviour
{
    //This hacky script merely holds a Vector3 of a position a character should never ever leave.

    public Vector3 talkingPosition;

    void Update()
    {
        transform.localPosition = talkingPosition;
    }
}
