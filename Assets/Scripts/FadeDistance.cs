﻿using UnityEngine;

public class FadeDistance : MonoBehaviour
{
    private GameObject player;
    private CanvasGroup bioImageCanvasGroup;
    private float minDistance = 4;
    private float maxDistance = 50;
    private Animator animator;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        bioImageCanvasGroup = transform.GetChild(0).GetComponent<CanvasGroup>();
        animator = transform.GetChild(0).GetComponent<Animator>();
    }

    private void Update()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        float alpha = minDistance / distance;
        if (distance > maxDistance)
            alpha = 0;
        if (distance < minDistance)
            alpha = 1;

        bioImageCanvasGroup.alpha = alpha;

        if (distance > minDistance && animator.GetBool("Out") == false)
        {
            animator.SetBool("In", false);
            animator.SetBool("Out", true);
            bioImageCanvasGroup.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
