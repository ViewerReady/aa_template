﻿using UnityEngine;

public class CardUrls : MonoBehaviour
{
    public string m_CampusVideoUrl;
    public string m_CampusWebsiteUrl;

    public string m_StudentVideoUrl;
    public string m_FacebookUrl;
    public string m_InstagramUrl;
    public string m_TwitchUrl;
    public string m_TwitterUrl;
    public string m_YoutubeUrl;
    public string m_TiktokUrl;
}
