﻿using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    private GameObject player;
    [SerializeField] private AudioSource m_Greeter_AudioSource;
    private bool m_Greeter_IsAudioPlaying;
    [SerializeField] private UISlideShow SlideShow_UI;
    [SerializeField] private string[] SlideShow_Videos;
    private MediaPlayer SlideShow_MediaPlayer;
    [SerializeField] private GameObject SlideShow_PlayButton;
    [SerializeField] private DisplayUGUI SlideShow_VideoPanel;
    private bool SlideShow_IsPlaying;
    [SerializeField] private GameObject XPS_Fiorano_15;
    [SerializeField] private MediaPlayer XPS_Fiorano_15_MediaPlayer;
    private bool XPS_Fiorano_15_IsPlaying;
    [SerializeField] private GameObject Viper_15;
    [SerializeField] private MediaPlayer Viper_15_MediaPlayer;
    private bool Viper_15_IsPlaying;
    [SerializeField] private MediaPlayer VideoPanel_MediaPlayer;
    [SerializeField] private GameObject[] VideoPanels;
    [SerializeField] private string[] VideoUrls;
    private int VideoPanelIndex = -1;
    [SerializeField] float forwardMovementOnTalk;
    [SerializeField] private MediaPlayer m_StudentVideoMediaPlayer;
    [SerializeField] private GameObject[] m_TemplateProps;
    [SerializeField] private EnvironmentAudioSwitcher m_IndoorAudioSwitcher;
    [SerializeField] private AudioClip[] m_IndoorAudioClips;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        m_Greeter_IsAudioPlaying = false;
        SlideShow_MediaPlayer = SlideShow_UI.GetComponent<MediaPlayer>();
        SlideShow_IsPlaying = false;
        XPS_Fiorano_15_IsPlaying = false;
        Viper_15_IsPlaying = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.transform.CompareTag("NPC1"))
                {
                    SetGreeterAudio();
                    return;
                }

                if (hit.transform.CompareTag("Canvas Button"))
                {
                    hit.transform.GetComponent<AudioSource>().Play();
                    ChooseTemplateScene(hit.transform);
                    return;
                }

                //if (hit.transform.CompareTag("StaffOnly"))
                //{
                //    NavigateWebPage("window.open(\"https://jobs.dell.com/\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Link_XPS_13"))
                //{
                //    NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/dell-laptops/new-xps-13-laptop/spd/xps-13-9300-laptop\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Link_XPS_15"))
                //{
                //    NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/dell-laptops/new-xps-15-laptop/spd/xps-15-9500-laptop\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Link_XPS_17"))
                //{
                //    NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/dell-laptops/new-xps-17-laptop/spd/xps-17-9700-laptop\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Link_AW_M15"))
                //{
                //    NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/cty/pdp/spd/alienware-m15-r2-laptop/wnm15r220s\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Link_AW_M17"))
                //{
                //    NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/cty/pdp/spd/alienware-m17-r2-laptop/wnm17r220s\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Link_AW_Aurora"))
                //{
                //    NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/desktop-computers/alienware-aurora-r9-gaming-desktop/spd/alienware-aurora-r9-desktop\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("Intel"))
                //{
                //    OpenLinkInNewTab("https://www.dell.com/en-us/member/shop/latest-intel-processors/cp/latest-intel-processors");
                //    //NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/latest-intel-processors/cp/latest-intel-processors\")");
                //    return;
                //}

                //if (hit.transform.CompareTag("NVIDIA"))
                //{
                //    OpenLinkInNewTab("https://www.dell.com/en-us/member/shop/nvidia/cp/nvidia\\");
                //    //NavigateWebPage("window.open(\"https://www.dell.com/en-us/member/shop/nvidia/cp/nvidia\")");
                //    return;
                //}

                if (hit.transform.CompareTag("PrevSlideShow"))
                {
                    PrevSlideShow();
                    return;
                }

                if (hit.transform.CompareTag("NextSlideShow"))
                {
                    NextSlideShow();
                    return;
                }

                if (hit.transform.CompareTag("PlaySlideShow"))
                {
                    if (SlideShow_IsPlaying == false)
                        PlaySlideShowVideo();
                    else
                        CloseSlideShowVideo();
                    return;
                }

                if (hit.transform.CompareTag("CloseSlideShow"))
                {
                    CloseSlideShowVideo();
                    return;
                }

                if (hit.transform.CompareTag("XPS_Fiorano_15"))
                {
                    if (XPS_Fiorano_15_IsPlaying == false)
                        XPS_Fiorano_15_Open();
                    else
                        XPS_Fiorano_15_Close();
                    return;
                }

                if (hit.transform.CompareTag("Viper_15"))
                {
                    if (Viper_15_IsPlaying == false)
                        Viper_15_Open();
                    else
                        Viper_15_Close();
                    return;
                }

                if (hit.transform.CompareTag("VideoPanel"))
                {
                    int.TryParse(hit.transform.name, out int index);
                    PlayWallVideo(index - 1);
                    return;
                }

                if (hit.transform.CompareTag("BioImagePanel"))
                {
                    if (hit.transform.name.Contains("BioImage"))
                        ZoomInBioPanel(hit.transform);
                    else
                    {
                        BioImageButtonClicked(hit.transform);
                    }

                    return;
                }

                if (hit.transform.CompareTag("Ashley"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Daniel"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Isabela"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Jake"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Joseph"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Lauren"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Miles"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
                else if (hit.transform.CompareTag("Nathan"))
                {
                    PlayAshleyAnimation(hit.transform);
                }
            }
        }
    }

    public void PlayWallVideo(int index)
    {
        if (VideoPanel_MediaPlayer.Control.IsPlaying()
            || VideoPanel_MediaPlayer.Control.IsPaused())
        {
            VideoPanels[index].transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            {
                VideoPanels[index].transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            }
        }

        if (VideoPanelIndex == index)
        {
            VideoPanels[index].GetComponent<ApplyToMaterial>().enabled = true;

            if (VideoPanel_MediaPlayer.Control.IsPaused())
            {
                VideoPanels[index].transform.GetChild(0).gameObject.SetActive(false);
                VideoPanel_MediaPlayer.Play();
            }
            else if (VideoPanel_MediaPlayer.Control.IsPlaying())
            {
                VideoPanels[index].transform.GetChild(0).gameObject.SetActive(true);
                VideoPanel_MediaPlayer.Pause();
            }
        }
        else
        {
            if (VideoPanelIndex != -1)
            {
                VideoPanels[VideoPanelIndex].transform.GetChild(0).gameObject.SetActive(true);
                VideoPanels[VideoPanelIndex].GetComponent<ApplyToMaterial>().enabled = false;
            }
            VideoPanel_MediaPlayer.CloseVideo();

            VideoPanels[index].transform.GetChild(0).gameObject.SetActive(false);
            VideoPanels[index].GetComponent<ApplyToMaterial>().enabled = true;

            VideoPanel_MediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VideoUrls[index], false);
            VideoPanel_MediaPlayer.Play();
        }

        VideoPanelIndex = index;
    }

    public void CloseWallVideo()
    {
        if (VideoPanelIndex != -1)
        {
            //VideoSeekPositions[VideoPanelIndex] = VideoPanel_MediaPlayer.Control.GetCurrentTimeMs();
            VideoPanel_MediaPlayer.CloseVideo();
            VideoPanels[VideoPanelIndex].transform.GetChild(0).gameObject.SetActive(true);
            VideoPanels[VideoPanelIndex].GetComponent<ApplyToMaterial>().enabled = false;
        }
    }

    private IEnumerator PlayDelayedVideo(bool isXPS)
    {
        yield return new WaitForSeconds(7);

        MediaPlayer mediaPlayer = isXPS == true ? XPS_Fiorano_15_MediaPlayer : Viper_15_MediaPlayer;
        mediaPlayer.OpenVideoFromFile(
            MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder,
            mediaPlayer.m_VideoPath, true);
        mediaPlayer.Play();
    }

    public void SetGreeterAudio()
    {
        if (!m_Greeter_IsAudioPlaying)
        {
            m_Greeter_AudioSource.Play();
            m_Greeter_IsAudioPlaying = true;
        }
        else
        {
            m_Greeter_AudioSource.Pause();
            m_Greeter_IsAudioPlaying = false;
        }
    }

    private void ChooseTemplateScene(Transform trans)
    {
        switch (trans.name)
        {
            case "University Button":
                {
                    ActivateTemplate(0);
                }
                break;
            case "Modern Chic Button":
                {
                    ActivateTemplate(1);
                }
                break;
            case "Industrial Button":
                {
                    ActivateTemplate(2);
                }
                break;
            default:
                break;
        }
    }

    private void ActivateTemplate(int templateIndex)
    {
        if (m_IndoorAudioSwitcher.GetComponent<AudioSource>().clip == m_IndoorAudioSwitcher.m_InteriorSound)
        {
            m_IndoorAudioSwitcher.GetComponent<AudioSource>().Stop();
            m_IndoorAudioSwitcher.m_InteriorSound = m_IndoorAudioClips[templateIndex];
            m_IndoorAudioSwitcher.GetComponent<AudioSource>().clip = m_IndoorAudioSwitcher.m_InteriorSound;
            m_IndoorAudioSwitcher.GetComponent<AudioSource>().Play();
        }
        else
        {
            m_IndoorAudioSwitcher.m_InteriorSound = m_IndoorAudioClips[templateIndex];
        }

        for (int i = 0; i < m_TemplateProps.Length; i++)
        {
            m_TemplateProps[i].SetActive(false);
        }

        m_TemplateProps[templateIndex].SetActive(true);

        MaterialSwitcher[] switchers = FindObjectsOfType<MaterialSwitcher>();
        for (int i = 0; i < switchers.Length; i ++)
        {
            switchers[i].GetComponent<MeshRenderer>().material = switchers[i].m_TemplateMaterials[templateIndex];
        }
    }

    public void PrevSlideShow()
    {
        CloseSlideShowVideo();
        SlideShow_UI.GetComponent<AudioSource>().Play();
        SlideShow_UI.OnPreviousImage();
        string indexName = SlideShow_UI.transform.GetChild(0).GetChild(SlideShow_Videos.Length - 1).name;
        int.TryParse(indexName, out int slideIndex);
        StartCoroutine(SetPlayButtonVisibilityAsync(slideIndex));
    }

    public void NextSlideShow()
    {
        CloseSlideShowVideo();
        SlideShow_UI.GetComponent<AudioSource>().Play();
        SlideShow_UI.OnNextImage();
        string indexName = SlideShow_UI.transform.GetChild(0).GetChild(SlideShow_Videos.Length - 1).name;
        int.TryParse(indexName, out int slideIndex);
        StartCoroutine(SetPlayButtonVisibilityAsync(slideIndex));

    }

    private IEnumerator SetPlayButtonVisibilityAsync(int slideIndex)
    {
        SlideShow_PlayButton.SetActive(false);
        yield return new WaitForSeconds(1);
        SlideShow_PlayButton.SetActive(true);
    }

    public void PlaySlideShowVideo()
    {
        SlideShow_MediaPlayer.CloseVideo();
        SlideShow_VideoPanel.enabled = true;
        string indexName = SlideShow_UI.transform.GetChild(0).GetChild(SlideShow_Videos.Length - 1).name;
        int.TryParse(indexName, out int videoIndex);
        if (videoIndex > 0)
        {
            SlideShow_MediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, SlideShow_Videos[videoIndex - 1], false);
            SlideShow_MediaPlayer.Play();
            SlideShow_IsPlaying = true;
        }
    }

    public void CloseSlideShowVideo()
    {
        SlideShow_IsPlaying = false;
        SlideShow_MediaPlayer.CloseVideo();
        SlideShow_VideoPanel.enabled = false;
    }

    public void XPS_Fiorano_15_Open()
    {
        XPS_Fiorano_15_IsPlaying = true;
        XPS_Fiorano_15.GetComponent<Animator>().ResetTrigger("Leave");
        XPS_Fiorano_15.GetComponent<Animator>().SetTrigger("Approach");
        StartCoroutine(PlayDelayedVideo(true));
    }

    public void XPS_Fiorano_15_Close()
    {
        XPS_Fiorano_15_MediaPlayer.CloseVideo();

        StartCoroutine(XPS_Fiorano_15_Close_With_Delay());
    }

    private IEnumerator XPS_Fiorano_15_Close_With_Delay()
    {
        yield return new WaitForSeconds(1);

        XPS_Fiorano_15_IsPlaying = false;
        XPS_Fiorano_15.GetComponent<Animator>().ResetTrigger("Approach");
        XPS_Fiorano_15.GetComponent<Animator>().SetTrigger("Leave");
    }

    public void Viper_15_Open()
    {
        Viper_15_IsPlaying = true;
        Viper_15.GetComponent<Animator>().ResetTrigger("Leave");
        Viper_15.GetComponent<Animator>().SetTrigger("Approach");
        StartCoroutine(PlayDelayedVideo(false));

    }

    public void Viper_15_Close()
    {
        Viper_15_MediaPlayer.CloseVideo();

        StartCoroutine(Viper_15_Close_With_Delay());
    }

    private IEnumerator Viper_15_Close_With_Delay()
    {
        yield return new WaitForSeconds(1);

        Viper_15_IsPlaying = false;
        Viper_15.GetComponent<Animator>().ResetTrigger("Approach");
        Viper_15.GetComponent<Animator>().SetTrigger("Leave");
    }

    //public void NavigateWebPage(string url)
    //{
    //    Application.ExternalEval(url);
    //}

    private void ZoomInBioPanel(Transform trans)
    {
        if (trans.GetComponent<Animator>().GetBool("In") == false)
        {
            trans.GetComponent<Animator>().SetBool("In", true);
            trans.GetComponent<Animator>().SetBool("Out", false);
            trans.GetComponent<AudioSource>().Play();

            trans.GetComponent<BoxCollider>().enabled = false;
        }
    }

    private void BioImageButtonClicked(Transform trans)
    {
        GameObject[] bioImagePanels = GameObject.FindGameObjectsWithTag("BioImagePanel");
        for (int i = 0; i < bioImagePanels.Length; i++)
        {
            if (trans == bioImagePanels[i] || !bioImagePanels[i].name.Contains("BioImage"))
                continue;
            foreach (Transform panel in bioImagePanels[i].transform)
            {
                panel.gameObject.SetActive(false);
            }
            bioImagePanels[i].transform.GetChild(0).gameObject.SetActive(true);
        }

        switch (trans.name)
        {
            case "Main Campus Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(0).gameObject.SetActive(false);
                    bioImageTrans.GetChild(1).gameObject.SetActive(true);
                }
                break;
            case "Main Student Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(0).gameObject.SetActive(false);
                    bioImageTrans.GetChild(3).gameObject.SetActive(true);
                }
                break;
            case "University Info Video Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(0).gameObject.SetActive(false);
                    bioImageTrans.GetChild(2).gameObject.SetActive(true);
                    m_StudentVideoMediaPlayer.OpenVideoFromFile(
                        MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder,
                        bioImageTrans.GetComponent<CardUrls>().m_CampusVideoUrl, true);
                    m_StudentVideoMediaPlayer.Play();
                }
                break;
            case "University Info Website Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_CampusWebsiteUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "University Info Back Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(0).gameObject.SetActive(true);
                    bioImageTrans.GetChild(1).gameObject.SetActive(false);
                }
                break;
            case "University Video Back Button":
                {
                    m_StudentVideoMediaPlayer.CloseVideo();
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(1).gameObject.SetActive(true);
                    bioImageTrans.GetChild(2).gameObject.SetActive(false);
                }
                break;
            case "Personal Info Video Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(3).gameObject.SetActive(false);
                    bioImageTrans.GetChild(4).gameObject.SetActive(true);
                    m_StudentVideoMediaPlayer.OpenVideoFromFile(
                        MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder,
                        bioImageTrans.GetComponent<CardUrls>().m_StudentVideoUrl, true);
                    m_StudentVideoMediaPlayer.Play();
                }
                break;
            case "Personal Info Instagram Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_InstagramUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "Personal Info Twitter Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_TwitterUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "Personal Info Youtube Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_YoutubeUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "Personal Info Tiktok Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_TiktokUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "Personal Info Facebook Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_FacebookUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "Personal Info Twitch Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    string destinationURL = bioImageTrans.GetComponent<CardUrls>().m_TwitchUrl;
                    OpenLinkInNewTab(destinationURL);
                }
                break;
            case "Personal Info Back Button":
                {
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(0).gameObject.SetActive(true);
                    bioImageTrans.GetChild(3).gameObject.SetActive(false);
                }
                break;
            case "Personal Video Back Button":
                {
                    m_StudentVideoMediaPlayer.CloseVideo();
                    Transform bioImageTrans = trans.parent.parent;
                    bioImageTrans.GetChild(3).gameObject.SetActive(true);
                    bioImageTrans.GetChild(4).gameObject.SetActive(false);
                }
                break;
            default:
                break;
        }
    }

    [SerializeField] private GameObject[] m_Avatars;
    private Coroutine stopCoroutine;

    public void PlayAshleyAnimation(Transform trans)
    {
        StoAllAnimation();

        trans.GetComponent<AudioSource>().Play();
        float audioLength = trans.GetComponent<AudioSource>().clip.length;
        stopCoroutine = StartCoroutine(StopAnimation(audioLength, trans.transform.parent.GetComponent<Animator>()));
        trans.transform.parent.GetComponent<Animator>().ResetTrigger("Idle");
        trans.transform.parent.GetComponent<Animator>().SetTrigger("Talk");
        trans.transform.parent.GetComponent<Animator>().enabled = true;
    }

    public void StoAllAnimation()
    {
        if (stopCoroutine != null)
            StopCoroutine(stopCoroutine);

        for (int i = 0; i < m_Avatars.Length; i++)
        {
            m_Avatars[i].GetComponent<AudioSource>().Stop();
            m_Avatars[i].transform.parent.GetComponent<Animator>().SetTrigger("Idle");
            m_Avatars[i].transform.parent.GetComponent<Animator>().ResetTrigger("Talk");
            m_Avatars[i].transform.parent.GetComponent<Animator>().enabled = false;

        }
    }

    private IEnumerator StopAnimation(float audioLength, Animator animator)
    {
        yield return new WaitForSeconds(audioLength + 0.5f);

        for (int i = 0; i < m_Avatars.Length; i++)
        {
            m_Avatars[i].transform.parent.GetComponent<Animator>().SetTrigger("Idle");
            m_Avatars[i].transform.parent.GetComponent<Animator>().ResetTrigger("Talk");
            m_Avatars[i].transform.parent.GetComponent<Animator>().enabled = false;

        }

        stopCoroutine = null;
    }

    public void OpenLinkInNewTab(string destURL)
    {
#if UNITY_EDITOR
        Debug.Log("This is the Unity editor. We're just gonna open the URL.");
        Application.OpenURL(destURL);
#elif !UNITY_EDITOR
            Debug.Log("ATTEMPTING TO OPEN IN NEW TAB, NOT UNITY EDITOR.");
            //Application.ExternalEval("window.open(\"" + destURL + "\",\"_blank\")");
            openWindow(destURL);
#endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
