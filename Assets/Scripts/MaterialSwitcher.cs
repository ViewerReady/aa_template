﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MaterialSwitcher : MonoBehaviour
{
    public Material[] m_TemplateMaterials;

    private void Start()
    {
        GetComponent<MeshRenderer>().material = m_TemplateMaterials[SceneManager.GetActiveScene().buildIndex];
    }
}
