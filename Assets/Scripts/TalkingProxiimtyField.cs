﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingProxiimtyField : MonoBehaviour
{
    [SerializeField] Animator characterAnimator;
    [SerializeField] AudioSource characterAudioSource;
    private Transform characterTransform;
    private Transform playerTransform;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        characterTransform = characterAnimator.GetComponent<Transform>();
    }

    void Update()
    {
        
        if (Vector3.Distance(playerTransform.position, characterTransform.position) >= 4 && characterAudioSource.isPlaying == true)
        {
            Debug.Log("Exiting " + characterAnimator.gameObject.name + "'s proximity. Stopping them from talking.");
            characterAudioSource.Stop();
            characterAnimator.SetTrigger("Idle");
            characterAnimator.ResetTrigger("Talk");
            if (characterTransform.name == "isabela_anim")
            {
                characterAnimator.enabled = false;
            }
        }
    }
}