﻿using UnityEngine;

public class XPS_Fiorano_15_Animation : MonoBehaviour
{
    private GameObject player;
    
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            GetComponent<Animator>().ResetTrigger("Leave");
            GetComponent<Animator>().SetTrigger("Approach");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            GetComponent<Animator>().ResetTrigger("Approach");
            GetComponent<Animator>().SetTrigger("Leave");
        }
    }
}