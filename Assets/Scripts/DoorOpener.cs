﻿using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    private GameObject player;
    [SerializeField]
    private AudioClip m_DoorOpenAudio;
    [SerializeField]
    private AudioClip m_DoorCloseAudio;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            GetComponent<Animator>().ResetTrigger("Close");
            GetComponent<Animator>().SetTrigger("Open");
            GetComponent<AudioSource>().clip = m_DoorOpenAudio;
            GetComponent<AudioSource>().PlayOneShot(m_DoorOpenAudio);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            GetComponent<Animator>().ResetTrigger("Open");
            GetComponent<Animator>().SetTrigger("Close");
            GetComponent<AudioSource>().clip = m_DoorCloseAudio;
            GetComponent<AudioSource>().PlayOneShot(m_DoorCloseAudio);
        }
    }
}