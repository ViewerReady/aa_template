﻿using UnityEngine;

public class InfiniteSculptureRotation : MonoBehaviour
{
    private void Update()
    {
        transform.Rotate(0, -30 * Time.deltaTime, 0);
    }
}
