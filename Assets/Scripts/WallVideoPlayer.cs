﻿using RenderHeads.Media.AVProVideo;
using UnityEngine;

public class WallVideoPlayer : MonoBehaviour
{
    private GameObject player;
    [SerializeField]
    private string[] videoUrls;
    [SerializeField]
    private GameObject[] videoPanels;
    private int videoIndex = -1;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void PlayVideo(int index)
    {
        videoIndex = index;

        for (int i = 0; i < videoPanels.Length; i++)
        {
            videoPanels[i].GetComponent<MediaPlayer>().CloseVideo();
        }
        videoPanels[index].GetComponent<MediaPlayer>().OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, videoUrls[index], true);
        videoPanels[index].GetComponent<MediaPlayer>().Play();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
            
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.transform.CompareTag("VideoPanel") 
                    && Vector3.Distance(player.transform.position, hit.transform.position) <= 10)
                {
                    PlayVideo(hit.transform.GetSiblingIndex());
                }
            }
        }
    }

    public void CloseVideo()
    {
        if (videoIndex != -1)
            videoPanels[videoIndex].GetComponent<MediaPlayer>().CloseVideo();
    }
}
