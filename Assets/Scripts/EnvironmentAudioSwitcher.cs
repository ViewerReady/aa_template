﻿using UnityEngine;

public class EnvironmentAudioSwitcher : MonoBehaviour
{
    private GameObject player;
    [SerializeField]
    private AudioClip m_BirdSound;
    [SerializeField]
    public AudioClip m_InteriorSound;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnCollisionEnter");
        if (other.gameObject == player)
        {
            GetComponent<AudioSource>().clip = m_InteriorSound;
            GetComponent<AudioSource>().Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("OnCollisionExit");
        if (other.gameObject == player)
        {
            GetComponent<AudioSource>().clip = m_BirdSound;
            GetComponent<AudioSource>().Play();
        }
    }
}
